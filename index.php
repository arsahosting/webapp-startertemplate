<?php

use controllers\IndexController;
use arsahosting\phpcore\system\Initializer;

define("FNAPI", TRUE);

// API Version
global $vapi;
$vapi = "0.6";

// Include config
include_once ("config.php");
if (file_exists('developmentConfig.php')) require 'developmentConfig.php';

// Required Libraries
// include_once ("./lib/hooks/basesystem.php");


// Load arsahosting phpcore
require 'lib/arsahosting/phpcore/init.php';

// Override default config
$CORECONFIG['Db']['Config'] = $db;

// Start arsahosting core initializer
(new Initializer())->handleRequest();

?>