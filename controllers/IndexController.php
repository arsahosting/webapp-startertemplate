<?php

namespace controllers;

if (! defined("FNAPI"))
	die("This file can not be accesed directly");

class IndexController extends BaseController {
	protected $model;

	public function __CONSTRUCT($controller, $method) {
		parent::__CONSTRUCT($controller, $method);
	}

	public function Index() {
		echo '<h1>Hello world</h1>';
	}

}
